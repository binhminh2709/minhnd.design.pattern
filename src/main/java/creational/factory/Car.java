package creational.factory;

public interface Car {
    void paintColor(String color);
    
    void setEngine(String engine);
    
    void setTyre(String tyre);
}
